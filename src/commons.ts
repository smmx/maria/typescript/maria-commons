// UTILS
import * as Datatypes from './datatypes/datatypes';
import {Form} from './forms/form';
import {ValueProcessor} from './values/values';

// CASTS
export function asBoolean(value: any): boolean | null {
    return Datatypes.asBoolean(value);
}

export function asInteger(value: any): number | null {
    return Datatypes.asInteger(value);
}

export function asFloat(value: any): number | null {
    return Datatypes.asFloat(value);
}

export function asString(value: any): string | null {
    return Datatypes.asString(value);
}

export function asDate(value: any): Date | null {
    return Datatypes.asDate(value);
}

// INPUTS
export function form(): Form {
    return new Form();
}

export function vp(blame: string | null = null): ValueProcessor<any, any> {
    return new ValueProcessor(
        (value: any): any => {
            return value;
        },
        blame
    );
}
