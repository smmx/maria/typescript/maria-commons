import {InputError, ValueProcessor} from '../values/values';

export class Form {

    fields: Map<string, FormField<any>>;

    _listeners: Array<(field: string, oldInput: any, newInput: any) => void>;

    constructor() {
        this.fields = new Map();

        this._listeners = [];
    }

    get allValid() {
        return this.areAllFieldsValid();
    }

    get dirtyValid() {
        return this.areDirtyFieldsValid();
    }

    get touchedValid() {
        return this.areTouchedFieldsValid();
    }

    // LISTENERS
    addListener(listener: (field: string, oldInput: any, newInput: any) => void) {
        this._listeners.push(listener);
    }

    // REST
    init(fieldname: string, value: any): Form {
        const field: FormField<any> | undefined = this.fields.get(fieldname);

        if (field === undefined) {
            throw new FormError('Field ' + fieldname + ' doesn\'t exist.');
        }

        // SET
        field.init(value);

        // RETURN
        return this;
    }

    initAll(values: { [key: string]: any }): Form {
        for (const [fieldname, field] of this.fields) {
            // GET VALUE
            const value = values[fieldname];

            // INIT
            if (value !== undefined) {
                this.fields.get(fieldname)?.init(value);
            }
        }

        return this;
    }

    getField(fieldname: string): FormField<any> | undefined {
        if (!this.fields.has(fieldname)) {
            throw new FormError('Field ' + fieldname + ' doesn\'t exist.');
        }

        return this.fields.get(fieldname);
    }

    registerField(fieldname: string, valueProcessor: ValueProcessor<any, any>, init: any = null): Form {
        const field = new FormField(valueProcessor);

        // PUT
        this.fields.set(fieldname, field);

        // INIT FIELD
        this.init(fieldname, init);

        // LISTEN
        field.addListener((oldInput, newInput) => {
            this._listeners.forEach(listener => {
                listener(fieldname, oldInput, newInput);
            });
        });

        // RETURN
        return this;
    }

    areAllFieldsValid(): boolean {
        for (const field of this.fields.values()) {
            if (!field.isValid()) {
                return false;
            }
        }

        return true;
    }

    areDirtyFieldsValid(): boolean {
        for (const field of this.fields.values()) {
            if (field.isDirty()) {
                if (!field.isValid()) {
                    return false;
                }
            }
        }

        return true;
    }

    areTouchedFieldsValid(): boolean {
        for (const field of this.fields.values()) {
            if (field.isTouched()) {
                if (!field.isValid()) {
                    return false;
                }
            }
        }

        return true;
    }

    getValue(fieldname: string): any {
        const field: FormField<any> | undefined = this.fields.get(fieldname);

        if (field === undefined) {
            return undefined
        }

        return field.getValue();
    }

    getAllValues(): { [key: string]: any } {
        const values: any = {};

        for (const [fieldname, field] of this.fields) {
            values[fieldname] = field.getValue();
        }

        return values;
    }

    getDirtyValues(): { [key: string]: any } {
        const values: any = {};

        for (const [fieldname, field] of this.fields) {
            if (field.isDirty()) {
                values[fieldname] = field.getValue();
            }
        }

        return values;
    }

    getTouchedValues(): { [key: string]: any } {
        const values: any = {};

        for (const [fieldname, field] of this.fields) {
            if (field.isTouched()) {
                values[fieldname] = field.getValue();
            }
        }

        return values;
    }

    setValue(fieldname: string, value: any): Form {
        const field: FormField<any> | undefined = this.fields.get(fieldname);

        if (field === undefined) {
            throw new FormError('Field ' + fieldname + ' doesn\'t exist.');
        }

        // SET
        field.setValue(value);

        // RETURN
        return this;
    }

    setAllValues(values: { [key: string]: any }): Form {
        for (const [fieldname, field] of this.fields) {
            // GET
            const value = values[fieldname];

            // SET
            if (value !== undefined) {
                this.fields.get(fieldname)?.setValue(value);
            }
        }

        // RETURN
        return this;
    }

    getErrors(): Map<string, string> {
        const errorMessages = new Map();

        for (const [fieldname, field] of this.fields) {
            errorMessages.set(fieldname, field.getError());
        }

        return errorMessages;
    }

}

export class FormField<T> {

    touched: boolean;
    dirty: boolean;
    _init: T | null;
    _input_error: InputError | null;
    _user_error: InputError | null;
    _processor: ValueProcessor<any, T>;
    _listeners: Array<(oldInput: any, newInput: any) => void>;

    constructor(valueProcessor: ValueProcessor<any, T>) {
        this.touched = false;
        this.dirty = false;

        this._input = null;
        this._init = null;
        this._value = null;
        this._input_error = null;
        this._user_error = null;
        this._processor = valueProcessor;

        this._listeners = [];

        // INIT VALUE
        this.init(null);
    }

    _input: any;

    get input() {
        return this.getInput();
    }

    set input(value) {
        this.setValue(value);
    }

    _value: T | null;

    get value() {
        return this.getValue();
    }

    set value(value) {
        this.setValue(value);
    }

    // GET/SET
    get valid() {
        return this.isValid();
    }

    get error() {
        return this.getError();
    }

    // LISTENERS
    addListener(listener: (oldInput: any, newInput: any) => void) {
        this._listeners.push(listener);
    }

    // SETTERS
    init(input: any) {
        this.processInput(input);
        this._init = this._value;
        this.dirty = false;
        this.touched = false;
    }

    setValue(input: any) {
        this.processInput(input);
        this.dirty = this._value !== this._init;
        this.touched = true;
    }

    setUserError(error: string) {
        if (error) {
            this._user_error = new InputError(error);
        } else {
            this._user_error = null;
        }
    }

    processInput(input: any) {
        // OLD
        const oldInput = this._input;
        const newInput = input;

        // UPDATE INPUT
        this._input = input;

        try {
            this._value = this._processor.apply(input);
            this._input_error = null;
        } catch (error) {
            this._value = null;
            this._input_error = new InputError(error.message);
        }

        // FIRE INPUT CHANGE
        if (oldInput !== newInput) {
            this._listeners.forEach(listener => {
                listener(oldInput, newInput);
            });
        }
    }

    // GETTERS
    getInput() {
        return this._input;
    }

    isTouched() {
        return this.touched;
    }

    isDirty() {
        return this.dirty;
    }

    isValid(): boolean {
        return !this._input_error && !this._user_error;
    }

    getValue(): T | null {
        if (this._input_error) {
            return null;
        }

        if (this._value != null) {
            return this._value;
        }

        return null;
    }

    getError(): string | null {
        if (this._input_error) {
            return this._input_error.message;
        }

        if (this._user_error) {
            return this._user_error.message;
        }

        return null;
    }

}

export class FormError extends Error {
    constructor(m: string) {
        super(m);

        Object.setPrototypeOf(this, FormError.prototype);
    }
}
