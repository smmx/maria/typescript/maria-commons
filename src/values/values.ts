import * as Datatypes from '../datatypes/datatypes';

export class ValueProcessor<I, O> {

    blame: string | null;
    parent: (input: I) => O;

    constructor(processor: (input: I) => O, blame: string | null = null) {
        this.blame = blame;
        this.parent = processor;
    }

    // PIPES
    check(predicate: (input: O) => boolean, errorMessage: string): ValueProcessor<I, O> {
        return new ValueProcessor<I, O>(
            (input: I): O => {
                const value: O = this.parent(input);

                if (!predicate(value)) {
                    throw new InputError(errorMessage);
                }

                return value;
            },
            this.blame
        );
    }

    when(predicate: (input: O) => boolean, then: O): ValueProcessor<I, O> {
        return new ValueProcessor<I, O>(
            (input: I): O => {
                const value: O = this.parent(input);

                if (predicate(value)) {
                    return then;
                }

                return value;
            },
            this.blame
        );
    }

    whenOrElse(predicate: (input: O) => boolean, then: O, orElse: O): ValueProcessor<I, O> {
        return new ValueProcessor<I, O>(
            (input: I): O => {
                const value: O = this.parent(input);

                if (predicate(value)) {
                    return then;
                }

                return orElse;
            },
            this.blame
        );
    }

    map<N>(mapper: (input: O) => N): ValueProcessor<I, N> {
        return new ValueProcessor<I, N>(
            (input: I): N => {
                const value: O = this.parent(input);
                return mapper(value);
            },
            this.blame
        );
    }

    peek(consumer: (input: O) => void): ValueProcessor<I, O> {
        return new ValueProcessor<I, O>(
            (input: I): O => {
                const value: O = this.parent(input);

                consumer(value);

                return value;
            },
            this.blame
        );
    }

    // CHECKS
    options(options: O[]): ValueProcessor<I, O> {
        return this.check(
            (input: O) => {
                return options.indexOf(input) > -1;
            },
            'Input is not a valid option.'
        );
    }

    notNull(): ValueProcessor<I, O> {
        return this.check(
            (input: O) => {
                return input !== null;
            },
            'Input can\'t be null.'
        );
    }

    ifNull(then: O): ValueProcessor<I, O> {
        return this.when(
            (input: O) => {
                return !input;
            },
            then
        );
    }

    // CAST
    asBoolean(): ValueProcessor<I, boolean | null> {
        return this.map((value: O) => Datatypes.asBoolean(value));
    }

    asInteger(): ValueProcessor<I, number | null> {
        return this.map((value: O) => Datatypes.asInteger(value));
    }

    asFloat(): ValueProcessor<I, number | null> {
        return this.map((value: O) => Datatypes.asFloat(value));
    }

    asString(): ValueProcessor<I, string | null> {
        return this.map((value: O) => Datatypes.asString(value));
    }

    asDate(): ValueProcessor<I, Date | null> {
        return this.map((value: O) => Datatypes.asDate(value));
    }

    // APPLY
    apply(input: I): O | null {
        try {
            return this.parent(input);
        } catch (error) {
            if (!this.blame) {
                throw new InputError(error.message);
            } else {
                throw new InputError(this.blame + '->' + error.message);
            }
        }

    }
}

export class InputError extends Error {
    constructor(m: string) {
        super(m);

        Object.setPrototypeOf(this, InputError.prototype);
    }
}
